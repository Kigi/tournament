#include <stdio.h>
#include "K_Chara_Data.h"

static int i, t;	 // i ＆ t：ループ制御用

//タイプ表示用
static char type[5][11] = {
	"パワー",
	"スピード",
	"ファイター",
	"トリック",
	"シューター"
};

/*-----------------------------------------------------
int型関数 K_check_chara
・返り値
	0：正常終了
	1：データにエラーがある
・引数
	なし
・説明
	全てのキャラをchara_no順に並べて表示する
	主キーの被りがないか、確認用の関数
-----------------------------------------------------*/
extern int K_check_chara(int check_kaburi[])
{
	static int kaburi_cnt[52] = { 0 };		 //kaburi_cnt：主キーに被りがあった場合のカウント用
	static int kaburi_flag[52] = { 0 };

	if (check_kaburi[0] == -1){
		for (i = 0; i < all_sum; i++){
			kaburi_flag[i] = i;
		}
		check_kaburi = kaburi_flag;
	}
	
	//全てのキャラの捜査
	for (i = 0; i < all_sum; i++)
	{
		//パワー系のキャラを捜査
		for (t = 0; t < power_sum; t++) {
			if (power[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
				if (power[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[i]++;
			}
		}
		//スピード系のキャラを捜査
		for (t = 0; t < speed_sum; t++) {
			if (speed[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
				if (speed[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[i]++;
			}
		}
		//ファイター系のキャラを捜査
		for (t = 0; t < fighter_sum; t++) {
			if (fighter[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
				if (fighter[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[i]++;
			}
		}
		//トリック系のキャラを捜査
		for (t = 0; t < trick_sum; t++) {
			if (trick[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
				if (trick[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[i]++;
			}
		}
		//シューター系のキャラを捜査
		for (t = 0; t < shooter_sum; t++) {
			if (shooter[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					shooter[t].chara_no,shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[0]);
				if (shooter[i].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[i]++;
			}
		}
	}

	printf("\n");
	//被りがあったか捜査
	for (i = 0, t = 0; i < all_sum; i++){
		//被りがあった場合は被っている番号とデータに無い番号を表示
		if (kaburi_cnt[i] != 1){
			t++;
			printf("%3d番：%d個被りがありました\n", t, kaburi_cnt[i]);
		}
	}
	//被りがなかった場合
	if (t == 0){
		printf("被りはありませんでした\n");
		return 0;
	}

	return 1;
}

/*-----------------------------------------------------
int型関数 K_print_chara
・返り値
	0以上
		探索データがどのグループに属してるかを返す
		0：パワー系　1：スピード系　２：ファイター系　３：トリック系　４：シューター
	-1
		データが存在しない
・引数
	int search_number
		キャラクターデータの中から探す番号
・説明
	キャラクターデータの中からsearch_numberで指定された番号の
	キャラクターを表示させる関数。戻り値がどこのグループに属してるか
	を返すため、探索用に使うこともできる。
-----------------------------------------------------*/
int K_print_chara(int search_number)
{
	//パワー系のキャラを捜査
	for (t = 0; t < power_sum; t++) {
		if (power[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
			if (power[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 0;
		}
	}
	//スピード系のキャラを捜査
	for (t = 0; t < speed_sum; t++) {
		if (speed[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
			if (speed[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 1;
		}
	}
	//ファイター系のキャラを捜査
	for (t = 0; t < fighter_sum; t++) {
		if (fighter[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
			if (fighter[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 2;
		}
	}
	//トリック系のキャラを捜査
	for (t = 0; t < trick_sum; t++) {
		if (trick[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
			if (trick[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 3;
		}
	}
	//シューター系のキャラを捜査
	for (t = 0; t < shooter_sum; t++) {
		if (shooter[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				shooter[t].chara_no, shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[0]);
			if (shooter[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 4;
		}
	}
	return -1;
}