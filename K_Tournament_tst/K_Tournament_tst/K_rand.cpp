#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "K_MT.h"
#include "K_Chara_Data.h"

/*-----------------------------------------------------
void型関数 K_make_tournament(int Tournament_sum)
・戻り値
	なし
・引数
	int tournament_sum
		トーナメント参加者の総数
	int dlc_flag
		DLCコンテンツを含めるか含めないかのフラグ
・説明
	マッチング確率表を元に、乱数を使いトーナメントを作成する
	内部アルゴリズムは最適解かつ毎回同じトーナメント表がでないよう
	焼きなまし法を用いている
-----------------------------------------------------*/
extern void K_make_tournament(const int tournament_sum,const int dlc_flag)
{
	/*-----------------------------------------------------
	変数宣言について
		int i, t
			ループ用
		int型 new_rand & last_rand
			新しく発生させた乱数と最後に参照した乱数
			コンピューターの処理速度による乱数の被りを防ぐ
		int型 tournament_str
			*Tournament初期化用。トーナメントの先頭要素が入っている
		int型 tournament_addres[2]
			トーナメントを作る２次元配列用の整数型配列
		int型 rand_map[5][5]
			各キャラタイプのマッチング確率表
		int型ポインタ *Tournament
			トーナメントを作るためのポインタ
			このポインタを用い擬似的な二次元配列を作る
			┏　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
			┃　配列イメージ　　　　　　														   
			┃		Tournament[tournament_sum][4];											
			┃　操作イメージ　　　															         　
			┃　	Tournament[tournament_addres[0]][tournament_addres[1]]　　　 
			┗　
			関数が呼び出された段階では用意するトーナメントの数が定まって
			ないため、ポインタを使い擬似的に配列を作成する
	-----------------------------------------------------*/
	int
		i_tst, t_tst,
		last_rand = -1, new_rand,
		tournament_start,
		tournament_address[2] = { 0 },
		rand_map[5][5] = { 0 },
		match_chara[all_sum] = { 0 };
	static int *Tournament = &tournament_start;
	//-----------------------------------------------------

	t_tst = 0;
	while (t_tst < tournament_sum) {
		//乱数シード初期化
		init_genrand((unsigned)clock());
		//乱数を変数new_randに代入
		new_rand = genrand_int32() % all_sum;
		if (new_rand != last_rand && new_rand >= 0) {
			/*printf("乱数発生回数：%d\n", t_tst + 1);
			printf("参照乱数：%d\n", new_rand);
			printf("最後に参照した乱数：%d\n", last_rand);*/
			if (match_chara[new_rand] != 1){
				last_rand = new_rand;
				K_print_chara(new_rand);
				match_chara[new_rand]++;
				//*Tournament = new_rand;
				t_tst++;
				Tournament++;
			}
		}
	}
	printf("\n");

	i_tst = 0;
	while (i_tst < tournament_sum){
		for (t_tst = 0; t_tst < 5 && i_tst < tournament_sum; t_tst++, i_tst++) {
			printf("%3d", match_chara[i_tst]);
		}
		printf("\n");
	} 
	//K_check_chara(&tournament_start);
	/*Tournament = &tournament_start;
	for (i_tst = 0; i_tst < tournament_sum; i_tst++){
		printf("%3d", *(Tournament + i_tst));
	}  */
}