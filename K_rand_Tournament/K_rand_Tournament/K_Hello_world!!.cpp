#include <stdio.h>
#include "K_Chara_Data.h"

int main(void)
{
	int
		flag = -1, x, y,
		*addrec[2];

	printf("トーナメントの要素数を入力してください：");
	scanf("%d", &x);
	printf("\n");

	printf("DLCを含めますか？\n");
	printf("Yes：1　No：0\n入力：");
	scanf("%d", &y);
	printf("\n");

	printf("Hello_world!!\n");
	//K_check_chara(-256, &flag, 1);
	//K_data_operation(4, NULL, -1);
	addrec[0] = K_make_matching(x, y);
	addrec[1] = K_make_tournament(x);
	K_tournament_process(addrec[1], addrec[0], x);

	return 0;
}