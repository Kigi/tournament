#include <stdio.h>
#include "K_Chara_Data.h"

static int i, t;	 // i ＆ t：ループ制御用

//タイプ表示用
static char type[5][11] = {
	"パワー",
	"スピード",
	"ファイター",
	"トリック",
	"シューター"
};

/*-----------------------------------------------------
int型関数 K_check_chara
・返り値
	0：正常終了
	1：データにエラーがある
・引数
	int check_sum
		チェックしてほしいデータの数
	int dimension
		データが二次元配列の場合、要素数を渡す
	int check_kaburi[]
		チェックしてほしいデータの先頭アドレス
		先頭要素に何が入っているかで動作を決定する
			１以上
				チェックしてほしいデータの先頭アドレス
			0以下
				初期データが正しいか確認する
・説明
	チェックしてほしいデータの先頭アドレスから
	主キーの被りがないか順に確認していく関数
-----------------------------------------------------*/
extern int K_check_chara(int check_sum, int check_kaburi[], int dimension)
{
	static int
		x,
		kaburi_cnt[all_sum],		 //kaburi_cnt：主キーに被りがあった場合のカウント用
		kaburi_flag[all_sum];	 //kaburi_flag：check_kaburiの先頭要素が-１の場合１〜all_sumまでの数字を挿入する

	//check_kaburiの先頭要素が-1ならデータの整合性の調査
	if (check_kaburi[0] == -1){
		for (i = 0; i < all_sum; i++){
			kaburi_flag[i] = i;
		}
		check_kaburi = kaburi_flag;
		check_sum = all_sum;
	}

	//配列要素の初期化
	for (i = 0; i < all_sum; i++){
		kaburi_cnt[i] = 0;
	}
	
	//全てのキャラの捜査
	for (i = 0 , x = 0; i < check_sum; i += dimension, x++)
	{
		//パワー系のキャラを捜査
		for (t = 0; t < power_sum; t++) {
			if (power[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
				if (power[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[x]++;
			}
		}
		//スピード系のキャラを捜査
		for (t = 0; t < speed_sum; t++) {
			if (speed[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
				if (speed[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[x]++;
			}
		}
		//ファイター系のキャラを捜査
		for (t = 0; t < fighter_sum; t++) {
			if (fighter[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
				if (fighter[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[x]++;
			}
		}
		//トリック系のキャラを捜査
		for (t = 0; t < trick_sum; t++) {
			if (trick[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
				if (trick[t].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[x]++;
			}
		}
		//シューター系のキャラを捜査
		for (t = 0; t < shooter_sum; t++) {
			if (shooter[t].chara_no == check_kaburi[i]) {
				printf("%3d %23s %32s\t%13s",
					shooter[t].chara_no,shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[0]);
				if (shooter[i].DLC_no == 1) {
					printf("　　DLCキャラです");
				}
				printf("\n");
				kaburi_cnt[x]++;
			}
		}
	}

	printf("\n");
	//被りがあったか捜査
	for (i = 0, t = 0; i < (check_sum / dimension); i++){
		//被りがあった場合は被っている番号とデータに無い番号を表示
		if (kaburi_cnt[i] != 1){
			t++;
			printf("%3d番：%d個被りがありました\n", i, kaburi_cnt[i]);
		}
	}
	//被りがなかった場合
	if (t == 0){
		printf("被りはありませんでした\n\n");
		return 0;
	}

	return 1;
}

/*-----------------------------------------------------
int型関数 K_print_chara
・返り値
	0以上
		探索データがどのグループに属してるかを返す
		0：パワー系　1：スピード系　２：ファイター系　３：トリック系　４：シューター
	-1
		データが存在しない
・引数
	int search_number
		キャラクターデータの中から探す番号
・説明
	キャラクターデータの中からsearch_numberで指定された番号の
	キャラクターを表示させる関数。戻り値がどこのグループに属してるか
	を返すため、探索用に使うこともできる。
-----------------------------------------------------*/
int K_print_chara(int search_number)
{
	//パワー系のキャラを捜査
	for (t = 0; t < power_sum; t++) {
		if (power[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
			if (power[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 0;
		}
	}
	//スピード系のキャラを捜査
	for (t = 0; t < speed_sum; t++) {
		if (speed[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
			if (speed[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 1;
		}
	}
	//ファイター系のキャラを捜査
	for (t = 0; t < fighter_sum; t++) {
		if (fighter[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
			if (fighter[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 2;
		}
	}
	//トリック系のキャラを捜査
	for (t = 0; t < trick_sum; t++) {
		if (trick[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
			if (trick[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 3;
		}
	}
	//シューター系のキャラを捜査
	for (t = 0; t < shooter_sum; t++) {
		if (shooter[t].chara_no == search_number) {
			printf("%3d %23s %32s\t%13s",
				shooter[t].chara_no, shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[0]);
			if (shooter[t].DLC_no == 1) {
				printf("　　DLCキャラです");
			}
			printf("\n");
			return 4;
		}
	}
	return -1;
}					

extern int K_data_operation(int mode, int check_kaburi[], int check_sum)
{
	static int
		x, dimension,
		kaburi_cnt[all_sum],		 //kaburi_cnt：主キーに被りがあった場合のカウント用
		kaburi_flag[all_sum];	 //kaburi_flag：check_sumが-１の場合１〜all_sumまでの数字を挿入する

	//配列要素、変数の初期化
	for (i = 0; i < all_sum; i++) {
		kaburi_cnt[i] = 0;
	}
	dimension = 3;

	 //check_sumが-1ならデータの整合性の調査
	if (check_sum == -1) {
		for (i = 0; i < all_sum; i++) {
			kaburi_flag[i] = i;
		}
		check_kaburi = kaburi_flag;
		check_sum = all_sum;
		dimension = 1;
	}

	//全てのキャラの捜査
	for (i = 0, x = 0; x < check_sum; i += dimension, x++)
	{
		//パワー系のキャラを捜査
		for (t = 0; t < power_sum; t++) {
			if (power[t].chara_no == check_kaburi[i]) {
				//モードによって処理内容が変わる
				switch (mode)
				{
				case 0: //プリントモードの処理
					printf("%3d %23s %32s\t%13s",
						power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
					if (power[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
					return 0;
				case 1: //キャラタイプモードの処理
					return 0;
				case 2: //キャラ作品モードの処理
					return power[t].sakuhin_no;
				case 3: //DLCモードの処理
					return power[t].DLC_no;
				default:
					printf("%3d %23s %32s\t%13s",
						power[t].chara_no, power[t].name, sakuhin[power[t].sakuhin_no], type[0]);
					if (power[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
				}
				kaburi_cnt[x]++;
			}
		}
		//スピード系のキャラを捜査
		for (t = 0; t < speed_sum; t++) {
			if (speed[t].chara_no == check_kaburi[i]) {
				//モードによって処理内容が変わる
				switch (mode)
				{
				case 0: //プリントモードの処理
					printf("%3d %23s %32s\t%13s",
						speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
					if (speed[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
					return 0;
				case 1: //キャラタイプモードの処理
					return 1;
				case 2: //キャラ作品モードの処理
					return speed[t].sakuhin_no;
				case 3: //DLCモードの処理
					return speed[t].DLC_no;
				default:
					printf("%3d %23s %32s\t%13s",
						speed[t].chara_no, speed[t].name, sakuhin[speed[t].sakuhin_no], type[1]);
					if (speed[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
				}
				kaburi_cnt[x]++;
			}
		}
		//ファイター系のキャラを捜査
		for (t = 0; t < fighter_sum; t++) {
			if (fighter[t].chara_no == check_kaburi[i]) {
				//モードによって処理内容が変わる
				switch (mode)
				{
				case 0: //プリントモードの処理
					printf("%3d %23s %32s\t%13s",
						fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
					if (fighter[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
					return 0;
				case 1: //キャラタイプモードの処理
					return 2;
				case 2: //キャラ作品モードの処理
					return fighter[t].sakuhin_no;
				case 3: //DLCモードの処理
					return fighter[t].DLC_no;
				default:
					printf("%3d %23s %32s\t%13s",
						fighter[t].chara_no, fighter[t].name, sakuhin[fighter[t].sakuhin_no], type[2]);
					if (fighter[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
				}
				kaburi_cnt[x]++;
			}
		}
		//トリック系のキャラを捜査
		for (t = 0; t < trick_sum; t++) {
			if (trick[t].chara_no == check_kaburi[i]) {
				//モードによって処理内容が変わる
				switch (mode)
				{
				case 0: //プリントモードの処理
					printf("%3d %23s %32s\t%13s",
						trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
					if (trick[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
					return 0;
				case 1: //キャラタイプモードの処理
					return 3;
				case 2: //キャラ作品モードの処理
					return trick[t].sakuhin_no;
				case 3: //DLCモードの処理
					return trick[t].DLC_no;
				default:
					printf("%3d %23s %32s\t%13s",
						trick[t].chara_no, trick[t].name, sakuhin[trick[t].sakuhin_no], type[3]);
					if (trick[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
				}
				kaburi_cnt[x]++;
			}
		}
		//シューター系のキャラを捜査
		for (t = 0; t < shooter_sum; t++) {
			if (shooter[t].chara_no == check_kaburi[i]) {
				//モードによって処理内容が変わる
				switch (mode)
				{
				case 0: //プリントモードの処理
					printf("%3d %23s %32s\t%13s",
						shooter[t].chara_no, shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[4]);
					if (shooter[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
					return 0;
				case 1: //キャラタイプモードの処理
					return 4;
				case 2: //キャラ作品モードの処理
					return shooter[t].sakuhin_no;
				case 3: //DLCモードの処理
					return shooter[t].DLC_no;
				default:
					printf("%3d %23s %32s\t%13s",
						shooter[t].chara_no, shooter[t].name, sakuhin[shooter[t].sakuhin_no], type[4]);
					if (shooter[t].DLC_no == 1) {
						printf("　　DLCキャラです");
					}
					printf("\n");
				}
				kaburi_cnt[x]++;
			}
		}
	}

	printf("\n");
	//被りがあったか捜査
	for (i = 0, t = 0; i < (check_sum / dimension); i++) {
		//被りがあった場合は被っている番号とデータに無い番号を表示
		if (kaburi_cnt[i] != 1) {
			t++;
			printf("%3d番：%d個被りがありました\n", i, kaburi_cnt[i]);
		}
	}
	//被りがなかった場合
	if (t == 0) {
		printf("被りはありませんでした\n\n");
		return 0;
	}

	return 1; 
}