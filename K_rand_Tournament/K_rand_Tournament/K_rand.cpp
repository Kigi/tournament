#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "K_MT.h"
#include "K_Chara_Data.h"

static int matching(int *Tournament_chara, int tournament_address, int rand, int x, int opus_flag);
static int kaburi_operation(int *kaburi, int tournament_sum, int dlc_flag, int match_chara[]);
static int opus_operation(const int match_not_opus[]);

/*-----------------------------------------------------
void型関数 K_make_tournament(int Tournament_sum)
・戻り値
	なし
・引数
	int tournament_sum
		トーナメント参加者の総数
	int dlc_flag
		DLCコンテンツを含めるか含めないかのフラグ
・説明
	トーナメントのマッチングキャラデータを作成する関数
	マッチングルールは以下のとおり
		同じ作品同士でなるべくマッチングしない
		キャラの相性の差がなるべく出ないようにマッチングさせる
		かつ、なるべく毎回異なるトーナメントができること
-----------------------------------------------------*/
extern int *K_make_matching(const int tournament_sum, const int dlc_flag)
{
	/*-----------------------------------------------------
	変数宣言について
		int型 i, t
			ループ用
		int order
			マッチング対象が１Pか２Pか判別用
		int kaburi
			キャラ別の最大マッチング数
		int型 new_rand, last_rand
			新しく発生させた乱数と最後に参照した乱数
			コンピューターの処理速度による乱数の被りを防ぐ
		int型配列 match_not_opus[]
			作品別のマッチング数
		int型配列 match_chara[]
			キャラ別のマッチング数
		int型 tournament_addres
			トーナメントを作る２次元配列用の変数
		int型ポインタ *Tournament
			トーナメントを作るためのポインタ
			このポインタを用い擬似的な二次元配列を作る
			┏　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
			┃　配列イメージ　　　　　　														   
			┃		Tournament[tournament_sum][3];											　 
			┗　
			関数が呼び出された段階では用意するトーナメントの数が定まって
			ないため、ポインタを使い擬似的に配列を作成する
	-----------------------------------------------------*/
	int
		i, t, order = 0,
		kaburi,
		new_rand, last_rand = -1,
		opus_flag = 0, kaburi_flag = 0,
		match_not_opus[opus_sum] = { 0 },
		match_chara[all_sum] = { 0 },
		tournament_address = 0,
		*Tournament_chara;
	//-----------------------------------------------------

	//配列要素数の確保&変数初期設定
	Tournament_chara = (int *)malloc(sizeof(int) * tournament_sum * 3);
	kaburi = tournament_sum / all_sum + 1;

	//DLCを含めない場合の変数処理
	if (dlc_flag == 0){
		//被り値の変更
		kaburi = tournament_sum / not_dlc_sum + 1;
		//DLCキャラがマッチングしない用
		for (i = 0; i < all_sum; i++){
			if (K_data_operation(3, &i, 1) == true){
				match_chara[i] = kaburi;
				match_not_opus[K_data_operation(2, &i, 1)]++;
			}
		}
	}

	/*i = 0;
	while (i < all_sum) {
		for (t = 0; t < 5 && i < all_sum; t++, i++) {
			printf("%3d", match_chara[i]);
		}
		printf("\n");
	}
	system("pause");
	printf("\n");		*/
 
	t = 0;
	while (t < tournament_sum) {
		//乱数シード初期化
		init_genrand((unsigned)clock());
		//乱数を変数new_randに代入
		new_rand = genrand_int32() % all_sum;

		//最後に取得した乱数と新しい乱数が異なる場合マッチング処理
		if (new_rand != last_rand && match_chara[new_rand] < kaburi) {
			//マッチング処理呼び出し
			order = matching(Tournament_chara, tournament_address, new_rand, order, opus_flag);
			//マッチングに成功した場合
			if (order == 0 || order == 1)
			{
				//作品別マッチング数の計算----------------------------------
				if (match_chara[new_rand] == kaburi) {
					match_not_opus[K_data_operation(2, &new_rand, 1)]++;
				} //---------------------------------------------------

				//変数処理---------------
				last_rand = new_rand;
				tournament_address++;
				match_chara[new_rand]++;
				t++;
				//---------------------

				//被り値の変更用 ----------------------------------------------
				if (kaburi_flag == 0 && kaburi != 1) {
					if (kaburi_operation(&kaburi, tournament_sum, dlc_flag, match_chara) == 1) {
						kaburi_flag = 1;
						kaburi--;
						//新しい被り数にすでに達しているキャラを捜査
						for (i = 0; i < all_sum; i++) {
							if (match_chara[i] == kaburi)
								match_not_opus[K_data_operation(2, &i, 1)]++;	 
						}
					}
				} //---------------------------------------------------------

				 //一つの作品しか残ってない場合用 ------------------
				if (opus_flag == 0) {
					opus_flag = opus_operation(match_not_opus);
				} //-----------------------------------------
			}
			else{
				order = 1;
			}
		}
	}
	//printf("\n");

	/*i = 0;
		while (i < all_sum) {
			for (t = 0; t < 5 && i < all_sum; t++, i++) {
				printf("%3d", match_chara[i]);
			}
			printf("\n");
		}
	system("pause");
	printf("\n");  */

	//トーナメントデータの整合性確認
	//K_check_chara((tournament_sum * 3), Tournament_chara, 3);
	
	return Tournament_chara;
}

/*-----------------------------------------------------
int型関数 matching(int *Tournament_chara, int tournament_address, 
									int rand, int order, int opus_flag)
・戻り値
	0
		トーナメント２Pがマッチング完了
	１
		トーナメント１Pがマッチング完了
	-1
		トーナメント２Pがアルゴリズムによりマッチング不可
・引数
	int *Tournament_chara
		トーナメントデータの先頭ポインタ
	int address
		トーナメントデータ捜査用のアドレス
	int number
		マッチング対象のキャラのキャラ番号
	int order
		１Pか２Pのマッチングかのフラグ
	int opus_flag
		残っているキャラ全てが同じ作品に属してるかどうかのフラグ
説明
	マッチングルールに指定されたようにマッチングする関数
	内部では山登り法と１Pと２Pが同じ作品のキャラかどうか評価して
	トーナメントのキャラデータを作成している
-----------------------------------------------------*/
static int matching(int *Tournament_chara, const int address, int number, const int order, const int opus_flag)
{
	int match_rand;
	static int
		match_type[2],
		match_chara_opus[2];
	const int
		rand_map[5][5] = {
			{ 40,70,60,20,30 },
			{ 70,40,20,50,60 },
			{ 60,20,40,30,20 },
			{ 20,50,30,40,60 },
			{ 30,60,20,60,40 },
	};

	if (order == 0)
	{
		//マッチングしたキャラのタイプを捜査
		match_type[0] = K_data_operation(1, &number, 1);
		//マッチングしたキャラの作品番号を捜査
		match_chara_opus[0] = K_data_operation(2, &number, 1);
		//printf("\nキャラタイプで起動：%d\n", match_type[0]);
		//配列データの作成--------------------------------------
		*(Tournament_chara + address * 3) = number;
		*(Tournament_chara + address * 3 + 1) = 0;
		*(Tournament_chara + address * 3 + 2) = true;
		//---------------------------------------------------
		return 1;
	}
	//後攻の要素
	else
	{
		//乱数初期化
		init_genrand((unsigned)clock());
		match_rand = genrand_int32() % 100 + 1;
		match_type[1] = K_data_operation(1, &number, 1);
		match_chara_opus[1] = K_data_operation(2, &number, 1);
		//printf("\nキャラタイプで起動：%d\n", match_type[1]);

		//確率表と照らしあわせ
		if (rand_map[match_type[0]][match_type[1]] >= match_rand) {
			if (opus_flag == -1 || match_chara_opus[0] != match_chara_opus[1])
			{
				//配列データの作成--------------------------------------
				*(Tournament_chara + address * 3) = number;
				*(Tournament_chara + address * 3 + 1) = 0;
				*(Tournament_chara + address * 3 + 2) = true;
				//---------------------------------------------------
				return 0;
			}
		}
		return -1;
	} //------------------------------------------------------------------------
}

/*---------------------------------------------------------------------------
int型関数 kaburi_operation(int *kaburi, int tournament_sum, int dlc_flag, int match_chara[])
・戻り値
	1
		マッチング最大数に達したキャラが上限に達した
	0
		まだマッチングに余裕あり
・引数
	int *kaburi
		現在の最大被り数
	int tournament_sum
		トーナメント参加者の総数
	int dlc_flag
		DLCを含めるか含めないかのフラグ
	int match_chara[]
		現在のキャラ別マッチング回数
・説明
	この関数ではマッチング最大数に達したキャラが上限に達してるかどうかを判断しています
	マッチング最大数とは、例えばトーナメント参加者が100人でDLCを含めない場合
	44キャラが２回マッチングし、4キャラが３回マッチングすることになります。
	この場合３回マッチングしたキャラが何人かを数え、もし４キャラになった場合１を返します
	なるべくマッチングする数、トーナメントの位置をランダムにする為めんどくさいことになってます
---------------------------------------------------------------------------*/
static int kaburi_operation(int *kaburi, int tournament_sum, int dlc_flag, int match_chara[])
{
	int match_flag = 0, i;

	//マッチング最大数に達したキャラの集計
	for (i = 0; i < all_sum; i++) {
		if (match_chara[i] == *kaburi) {
			match_flag++;
		}
	}

	//もしマッチング最大数に達したキャラが上限に達したら
	//DLCなしの場合の条件式
	if (dlc_flag == 0 && match_flag == (tournament_sum % not_dlc_sum) + 4) {
		return 1;
	}
	//DLCありの場合の条件式
	else if (match_flag == tournament_sum % all_sum) {
		return 1;
	}

	return 0;
}

/*-----------------------------------------------------
int型関数 opus_operation(const int match_not_opus[])
・戻り値
	0 
		残ってる作品が複数存在する
	-1
		残ってる作品が一つしかない
・引数
	int match_not_opus[]
		現在作品別で全てマッチングし終えたキャラの総数の配列
説明
	現在で残っている作品がどれだけ残っているかを確認する関数
	トーナメントのルールとしてなるべく同じ作品同士でマッチングしないが、
	残っているキャラの作品が一つしかない場合エラーが出るため
	この関数で残っている作品を確認する必要がある
-----------------------------------------------------*/
static int opus_operation(const int match_not_opus[])
{
	int t, i;
	const int max_opus[opus_sum] = {
		super_mario,yossi,wario,DKong,zeruda,metroid,parutena,
		mother,emblem,kirby,starfox,pokemon,gameWatch,punchout,
		duckhunt,famikon,fzero,doumori,pikumin,wii_fit,xenoblade,
		sonic,rockman,pacman,streetfighter
	};

	t = 0;
	for (i = 0; i < opus_sum; i++) {
		if (max_opus[i] != match_not_opus[i]) {
			t++;
		}
	}
	if (t == 1) {
		return -1;
	}
	else {
		return 0;
	}
}