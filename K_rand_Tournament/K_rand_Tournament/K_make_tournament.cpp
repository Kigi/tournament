#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "K_Chara_Data.h"

static void K_tournament_print(int tournament_element, int *tournament_chara);

/*-----------------------------------------------------
int型ポインタ関数　K_make_tournament
・引数
	int tournament_sum
		トーナメント参加者の数
・戻り値
		トーナメントデータの先頭アドレス
・説明
	トーナメントのデータを作成する関数
-----------------------------------------------------*/
extern int *K_make_tournament(int tournament_sum)
{
	static int
		i,
		Tounament_data_address = 0,
		*Tournament_data;
	//------------------------------------------------

	i = 1;
	//配列要素の確保
	for (;;){
		if (pow(2, i) >= tournament_sum) {
			Tournament_data = (int *)malloc(sizeof(int) * i * 2);
			break;
		}
		i++;
	}
	
	while (tournament_sum != 1)
	{
		*(Tournament_data + 2 * Tounament_data_address) = tournament_sum / 2;
		*(Tournament_data + 2 * Tounament_data_address + 1) = tournament_sum % 2;
		tournament_sum =
			*(Tournament_data + 2 * Tounament_data_address) +
			*(Tournament_data + 2 * Tounament_data_address + 1);
		Tounament_data_address++;
	}

	return Tournament_data;
}

/*-----------------------------------------------------
void型関数　K_make_tournament
・引数
	int tournament_sum
		トーナメント参加者の数
・戻り値
	トーナメントデータの先頭アドレス
・説明
	トーナメントのデータを作成する関数
-----------------------------------------------------*/
extern void K_tournament_process(int *tournament_data, int *tournament_chara, int tournament_sum)
{
	static int
		i, t, x,
		Count = 0, hierarchy,
		tournament_data_address,
		tournament_chara_address;

	i = 0;
	for (;;) {
		if (pow(2, i) >= tournament_sum) {
			hierarchy = i;
			break;
		}
		i++;
	}

	//トーナメントデータ操作用変数初期化
	tournament_data_address = 0;
	for (x = 0; x < hierarchy; x++)
	{
		printf("第%2d回戦\n\n", x + 1);
		//トーナメントが奇数の場合シード位置のキャラを敗退扱いにする
		if (*(tournament_data + tournament_data_address +1) == 1){
			i = 0, t = 0;
			tournament_chara_address = 3 * tournament_sum - 3;
			//一番端のキャラがシード対象の場合
			if (Count == 0){
				while (tournament_sum > i) {
					if (*(tournament_chara + tournament_chara_address + 2) == true) {
						*(tournament_chara + tournament_chara_address + 2) = false;
						Count = 1;
						break;
					}
					tournament_chara_address -= 3;
					i++;
				}
			}
			//それ以外の場合
			else{
				while (tournament_sum > i){
					if (*(tournament_chara + tournament_chara_address + 2) == true) {
						t++;
						if (t == 3){
							*(tournament_chara + tournament_chara_address + 2) = false;
							break;
						}
					}
					tournament_chara_address -= 3;
					i++;
				}
			}
			//トーナメント処理の呼び出し
			K_tournament_print(*(tournament_data + tournament_data_address), tournament_chara);
			*(tournament_chara + tournament_chara_address + 2) = true;
			tournament_chara_address = 0;
		}
		//トーナメントの要素が偶数の場合なにもせずにトーナメントを呼び出す
		else{
			K_tournament_print(*(tournament_data + tournament_data_address), tournament_chara);
		}
		tournament_data_address += 2;
	}

	return;
}

/*-----------------------------------------------------
void型関数　K_make_tournament
・引数
	int tournament_element
		トーナメントの要素の数
	int *tournament_chara
		マッチングキャラの配列データの先頭アドレス
・戻り値
	なし
・説明
	トーナメントに従ってキャラのマッチングを行う関数
-----------------------------------------------------*/
static void K_tournament_print(int tournament_element, int *tournament_chara)
{
	static int
		i, t,
		matching_chara[2],
		*matching_chara_address[2],
		tournament_chara_address;

	//変数の初期化
	tournament_chara_address = 0;


	for (t = 0; t < tournament_element; t++)
	{
		i = 0;
		//キャラマッチングデータから対象キャラを選択
		while (i != 2){
			if (*(tournament_chara + tournament_chara_address + 2) == true){
				matching_chara[i] = *(tournament_chara + tournament_chara_address);
				matching_chara_address[i] = tournament_chara + tournament_chara_address;
				i++;
			}
			tournament_chara_address += 3;
		}

		//マッチングキャラの表示
		K_data_operation(0, matching_chara, 1);
		K_data_operation(0, matching_chara + 1, 1);
		/*K_print_chara(matching_chara[0]);
		K_print_chara(matching_chara[1]);*/

		for (;;){
			printf("勝ったキャラを選択してください(1~2)：");
			scanf("%d", &i);
			if (i == 1) {
				*(matching_chara_address[1] + 2) = false;
				*(matching_chara_address[0] + 1) += 1;
				break;
			}
			else if (i == 2){
				*(matching_chara_address[0] + 2) = false;
				*(matching_chara_address[1] + 1) += 1;
				break;
			}
			printf("入力は1〜2の範囲でお願いします\n");
		}
		printf("\n");
	}

	return;
}