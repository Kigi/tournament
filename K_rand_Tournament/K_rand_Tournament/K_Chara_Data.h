#ifndef K_Chara_Data
#define K_Chara_Data

#define false -1
#define true 1

#define opus_sum 25		//出演作品の総数
#define power_sum 6		//パワー系の総数
#define speed_sum 10	//スピード系の総数
#define fighter_sum 15	//ファイター系の総数
#define trick_sum 14		//トリック系の総数
#define shooter_sum 7	//シューター系の総数
#define not_dlc_sum 48	//DLC含めないキャラ総数
#define all_sum 52		//DLCを含めるキャラ総数

/*作品別用デファイン文　（勘弁してくれ.......。）
　それぞれの作品で出ているキャラの総数を定数化 
 　わかりにくいのは......目をつぶってください。
   ※データは2015年11月21日現在のもの*/
#define super_mario 7
#define yossi 1
#define wario 1
#define DKong 2
#define zeruda 5
#define metroid 2
#define parutena 3
#define mother 2
#define emblem 5
#define kirby 3
#define starfox 2
#define pokemon 6
#define gameWatch 1
#define punchout 1
#define duckhunt 1
#define famikon 1
#define fzero 1
#define doumori 1
#define pikumin 1
#define wii_fit 1
#define xenoblade 1
#define sonic 1
#define rockman 1
#define pacman 1
#define streetfighter 1

/*-----------------------------------------------------
文字型ポインタ配列 sakuhin
・説明
	登場キャラクターのそれぞれの作品名一覧（2015年11月21日現在）
-----------------------------------------------------*/
static char *sakuhin[opus_sum] =
{
	"スーパーマリオ",
	"ヨッシーシリーズ",
	"ワリオシリーズ",
	"ドンキーコング",
	"ゼルダの伝説",
	"メトロイド",
	"光神話パルテナの鏡",
	"mother",
	"ファイアーエムブレム",
	"星のカービィ",
	"スターフォックス",
	"ポケットモンスター",
	"ゲームアンドウォッチ",
	"パンチアウト!!",
	"ダックハント",
	"ファミリコンピューターロボット",
	"F-ZERO",
	"どうぶつの森",
	"ピクミン",
	"Wii Fit",
	"ゼノブレイド",
	"ソニック",
	"ロックマン",
	"パックマン",
	"ストリートファイター",
};
//-----------------------------------------------------

/*-----------------------------------------------------
構造体　chara
tyepedef宣言　deta_list
・説明
	ゲームに登場するキャラクターのデータ
		int chara_no;
			全てのキャラに割り当てられた番号。データベースで言う主キー
		char *name
			キャラ名
		int sakuhin_no;
			文字型配列 sakuhinで宣言した作品名一覧に対応した番号
		int DLC_no;
			キャラクターがDLCかそうでないかのフラグ
			trueならDLCキャラクター、falseならデフォルトキャラクター
-----------------------------------------------------*/
typedef struct chara {
	int chara_no;
	char *name;
	int sakuhin_no;
	int DLC_no;
}deta_list;
//-----------------------------------------------------

/*-----------------------------------------------------
構造体 chara型 power
・説明
	構造体chara型で作られたキャラデータ
	データは全てパワー系に属するキャラクター
-----------------------------------------------------*/
static deta_list power[6] =
{
	{ 4,"クッパ",0,false },
	{ 9,"ドンキーコング",3,false },
	{ 14,"ガノンドロフ",4,false },
	{ 25,"アイク",8,false },
	{ 30,"デデデ",9,false },
	{ 36,"リザードン",11,false },
};
//-----------------------------------------------------

/*-----------------------------------------------------
構造体 chara型 speed
・説明
	構造体chara型で作られたキャラデータ
	データは全てスピード系に属するキャラクター
-----------------------------------------------------*/
static deta_list speed[speed_sum] =
{
	{ 10,"ディディーコング",3,false },
	{ 13,"シーク",4,false },
	{ 17,"ゼロスーツサムス",5,false },
	{ 23,"マルス",8,false },
	{ 27,"ルキナ",8,false },
	{ 29,"メタナイト",9,false },
	{ 31,"フォックス",10,false },
	{ 33,"ピカチュウ",11,false },
	{ 38,"ゲッコウガ",11,false },
	{ 48,"ソニック",21,false },
};
//-----------------------------------------------------

/*-----------------------------------------------------
構造体 chara型 fighter
・説明
	構造体chara型で作られたキャラデータ
	データは全てファイター系に属するキャラクター
-----------------------------------------------------*/
static deta_list fighter[fighter_sum] =
{
	{ 0,"マリオ",0,false },
	{ 2,"ドクターマリオ",0,false },
	{ 7,"ヨッシー",1,false },
	{ 8,"ワリオ",2,false },
	{ 11,"リンク",4,false },
	{ 18,"ピット",6,false },
	{ 19,"ブラックピット",6,false },
	{ 20,"パルテナ",6,false },
	{ 24,"ロイ",8,true },
	{ 32,"ファルコ",10,false },
	{ 37,"ルカリオ",11,false },
	{ 40,"リトル・マック",13,false },
	{ 42,"ロボット",15,false },
	{ 43,"キャプテン・ファルコン",16,false },
	{ 51,"リュウ",24,true },
};
//-----------------------------------------------------

/*-----------------------------------------------------
構造体 chara型 trick
・説明
	構造体chara型で作られたキャラデータ
	データは全てトリック系に属するキャラクター
-----------------------------------------------------*/
static deta_list trick[trick_sum] =
{
	{ 1,"ルイージ",0,false },
	{ 3,"ピーチ",0,false },
	{ 5,"ロゼッタ＆チコ",0,false },
	{ 6,"クッパJr",0,false },
	{ 15,"トゥーンリンク",4,false },
	{ 21,"ネス",7,false },
	{ 22,"リュカ",7,true },
	{ 28,"カービィ",9,false },
	{ 34,"プリン",11,false },
	{ 35,"ミュウツー",11,true },
	{ 39,"Mr . ゲーム＆ウォッチ",12,false },
	{ 45,"ピクミン＆オリマー",18,false },
	{ 47,"シュルク",20,false },
	{ 50,"パックマン",23,false },
};
//-----------------------------------------------------

/*-----------------------------------------------------
構造体 chara型 shooter
・説明
	構造体chara型で作られたキャラデータ
	データは全てシューター系に属するキャラクター
-----------------------------------------------------*/
static deta_list shooter[shooter_sum] =
{
	{ 12,"ゼルダ",4,false },
	{ 16,"サムス",5,false },
	{ 26,"ルフレ",8,false },
	{ 41,"ダックハント",14,false },
	{ 44,"むらびと",17,false },
	{ 46,"Wii Fitトレーナー",19,false },
	{ 49,"ロックマン",22,false },
};
//-----------------------------------------------------

#endif

extern int K_check_chara(int check_sum, int check_kaburi[], int dimension);
extern int K_print_chara(int search_number);
extern int K_data_operation(int mode, int check_kaburi[], int check_sum);
extern int *K_make_matching(const int tournament_sum, const int dlc_flag);
extern int *K_make_tournament(int tournament_sum);
extern void K_tournament_process(int *tournament_data, int *tournament_chara, int tournament_sum);